package edu.westga.cs1302.project2.viewmodel;

import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.time.LocalDate;

import java.util.Collections;
import java.util.List;

import edu.westga.cs1302.project2.controller.CalendarBookController;
import edu.westga.cs1302.project2.datatier.TextFileLoader;
import edu.westga.cs1302.project2.datatier.TextFileWriter;
import edu.westga.cs1302.project2.datatier.TextLoader;
import edu.westga.cs1302.project2.datatier.TextUrlLoader;
import edu.westga.cs1302.project2.model.CalendarBook;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.TypeDateComparator;
import edu.westga.cs1302.project2.model.Validator;
import edu.westga.cs1302.project2.model.utils.Utils;
import edu.westga.cs1302.project2.resources.UI;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The controller manages all the events.
 * 
 * @author	CS1302
 * @version	Fall 2020
 */
public class GuiViewModel {

	/* stores all the events loaded or created */
	private CalendarBookController calendarBookController;

	private StringProperty descriptionProperty;
	private ObjectProperty<LocalDate> dateProperty;
	private StringProperty nYearProperty;
	private StringProperty sigLevelProperty;
	private StringProperty errorMessageProperty;
	/* stores the events to be shown in the listview (could be all the events or search result)*/
	private ObservableList<Event> eventList; 

	/**
	 * Creates an instance of the GuiViewModel class.
	 * 
	 * @precondition: none
	 * @postcondition: All the needed fields are initialized.
	 */
	public GuiViewModel() {
		this.calendarBookController = new CalendarBookController();

		this.descriptionProperty = new SimpleStringProperty("");

		this.dateProperty = new SimpleObjectProperty<LocalDate>();
		this.nYearProperty = new SimpleStringProperty("");
		this.sigLevelProperty = new SimpleStringProperty("");

		this.errorMessageProperty = new SimpleStringProperty("");
		this.eventList = FXCollections.observableArrayList();

	}

	/**
	 * Get the CalendarBook object.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the CalendarBook object.
	 */
	public CalendarBook getCalendarBook() {
		return this.calendarBookController.getCalendarBook();
	}

	/**
	 * Get the description property.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the string property
	 */
	public StringProperty descriptionProperty() {
		return this.descriptionProperty;
	}

	/**
	 * Get the date property.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the date property
	 */
	public ObjectProperty<LocalDate> dateProperty() {
		return this.dateProperty;
	}

	/**
	 * Get the nYear property.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the nYear property
	 */
	public StringProperty nYearProperty() {
		return this.nYearProperty;
	}

	/**
	 * Get the sigLevel property.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the sigLevel property
	 */
	public StringProperty sigLevelProperty() {
		return this.sigLevelProperty;
	}

	/**
	 * Get the errorMessage property.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the errorMessage property
	 */
	public StringProperty errorMessageProperty() {
		return this.errorMessageProperty;
	}

	/**
	 * Get the eventListProperty.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the eventListProperty
	 */
	public ObservableList<Event> eventListProperty() {
		return this.eventList;
	}

	/**
	 * add a new event object.
	 * 
	 * @param type the type of the Event to be added
	 * 
	 * @precondition: none
	 * @postcondition: this.eventListProperty().size() ==
	 *                 this.eventListProperty().size()@pre +1
	 * 
	 * @return true if the event is added successfully, false otherwise.
	 */
	public boolean addEvent(String type) {

		Validator validator = new Validator();

		String description = validator.validateDescription(this.descriptionProperty.get());

		LocalDate date = validator.validateDate(this.dateProperty.get());

		type = validator.validateType(type);

		String nYearString = this.nYearProperty.get();
		String sigLevelString = this.sigLevelProperty.get();

		int nyear = 0;
		int sigLevel = 0;

		if (description == null || date == null || type == null) {
			this.errorMessageProperty.set(validator.getErrorMessage());
			return false;
		}
		
		try {
			
			if (type.equalsIgnoreCase(EventType.NYEAR.toString())) {
				nyear = this.getValidatedValue(validator, nYearString);
			}
		
			if (type.equalsIgnoreCase(EventType.ONETIME.toString())) {
				sigLevel = this.getValidatedValue(validator, sigLevelString);
			} 
			
			Event newEvent = Utils.createEvent(type, date, description, nyear, sigLevel);
			if (this.calendarBookController.getCalendarBook().add(newEvent)) {
				this.eventList.setAll(this.calendarBookController.getCalendarBook().getEvents());
				return true;
			} else {
				this.errorMessageProperty.set("the event was not added");
				return false;
			}
		
		} catch (Exception exception) {
			this.errorMessageProperty.set(exception.getMessage());
			return false;
		}
	}

	private int getValidatedValue(Validator validator, String valueString) throws Exception {
		valueString = validator.validateTextField(valueString);
		
		if (valueString == null) {
			throw new Exception(validator.getErrorMessage());
		}
		
		int valueInt = Integer.parseInt(valueString);
		return valueInt;
	}
	
	/**
	 * Load the events from a file.
	 * 
	 * @param file the file from which events are to be loaded
	 * 
	 * @precondition: none
	 * @postcondition: the calendarBook is unchanged if the file is empty;otherwise,
	 *                 all the non-duplicate events of the file are added to the
	 *                 calendarBook and the events are shown in the ListView.
	 * 
	 * @throws IOException
	 */
	public void loadFile(File file) throws IOException {

		// TODO:
		// Part 1
		// it creates a TextFileLoader object and calls the setEventList
		TextFileLoader fileLoader = new TextFileLoader(file);
		this.setEventList(fileLoader);

	}

	/* TODO: uncomment the method header below
	   Part 1
	   Note that the setEventList method takes an Interface as its parameter. Because 
	   of polymorphism, this allows us to pass in any object that implements the
	   TextLoader to the method.
	
	   Call the loadData method of the fileLoader object to load the events from a
	   file and pass the result to setEventList
	*/
	private void setEventList(TextLoader fileLoader) throws IOException {
		this.setEventList(fileLoader.loadData());
	}

	/* TODO:
	   Part 1
	   Go through the event list and add the event to the calendarBook only if
	   the event is not already present
	   if the event is present, set "Duplicate events are not added!" in
	   the errorMessageProperty
	   Once the list is processed, clear the eventList and add all the 
	   calendarBook's events to the eventList.
	*/
	private void setEventList(List<Event> events) {
		for (Event anEvent : events) {
			if (!this.calendarBookController.getCalendarBook().add(anEvent)) {
				this.errorMessageProperty.set("Duplicate events are not added!");
			}
		}
		this.eventList.clear();
		this.eventList.setAll(this.calendarBookController.getCalendarBook().getEvents());
	}

	/**
	 * TODO: Part 2 Load the events from a URL resource.
	 * 
	 * @param url the url to the resource where the data is stored
	 * 
	 * @precondition: none
	 * @postcondition: the calendarBook is unchanged if the resource at the
	 *                 specified url is empty; otherwise, all the non-duplicate
	 *                 events specified in the URL resource are added to the
	 *                 calendarBook and the events are shown in the ListView.
	 * @throws IOException
	 */
	public void loadUrl(URL url) throws IOException {
		TextUrlLoader urlLoader = new TextUrlLoader(url);
		List<Event> events = urlLoader.loadData();
		this.setEventList(events);
	}

	/**
	 * 
	 * reload the calendarBook events into the ListView
	 * 
	 * @precondition: none
	 * @postcondition: the calendarBook events are loaded into the ListView
	 * 
	 */
	public void reloadEvents() {
		if (!this.isEmpty(this.calendarBookController.getCalendarBook().getEvents())) {
			this.eventList.clear();
			this.eventList.setAll(this.calendarBookController.getCalendarBook().getEvents());
		} 
	}

	/**
	 * TODO: Part 1 Save the events in the ListView into a file represented by
	 * filename
	 * 
	 * @param file name of the file that the events are saved to.
	 * 
	 * @precondition: none
	 * @postcondition: file exists if there are events in the ListView
	 * 
	 * @throws IOException
	 * 
	 */
	public void saveFile(File file) throws IOException {
		TextFileWriter fileWriter = new TextFileWriter(file);
		fileWriter.save(this.calendarBookController.getCalendarBook().getEvents());
	}

	/**
	 * TODO: Part 2: Save the events in the ListView in natural ordering in four
	 * groups (Daily, Monthly, NYear, Onetime) in the format of the following into a
	 * file represented by filename.
	 * 
	 * TYPE: size = xx yyyy-mm-dd, description yyyy-mm-dd, description
	 * 
	 * @param file name of the file that the events are saved to.
	 * 
	 * @precondition: none
	 * @postcondition: file exists if there are events in the ListView
	 * 
	 * @throws IOException
	 * 
	 */
	public void saveFile2(File file) throws IOException {
		this.sortByDate();
		TextFileWriter fileWriter = new TextFileWriter(file);
		fileWriter.save2(this.calendarBookController.getCalendarBook().getEvents());
	}
	
	/**
	 * saves the current list of events by their date
	 * 
	 * Events on yyyy-mm-dd
	 * Event 1: description
	 * Event 2: description
	 * ...
	 * ~~~GROUPED BY EVENT TYPE~~~
	 * TYPE: size = xx yyyy-mm-dd, description yyyy-mm-dd, description
	 * 
	 * @param file name of the file that the events are saved to.
	 * 
	 * @precondition: none
	 * @postcondition: file exists if there are events in the ListView
	 * 
	 * @throws IOException
	 * 
	 */
	public void saveFile3(File file) throws IOException {
		this.sortByDate();
		TextFileWriter fileWriter = new TextFileWriter(file);
		fileWriter.save3(this.calendarBookController.getCalendarBook().getEvents());
	}

	/**
	 * The method shows in the ListView all the events that occur on the given date
	 * or contain the given description or occur on the given date and contain the
	 * given description (if both fields are filled).
	 * 
	 * 
	 * The user may pick a date in the datePicker, or enter a description, or pick a
	 * date and enter a description in the GUI.
	 * 
	 * The method looks for events in the calendarBook that either occur on the date
	 * specified by the date that the user picked (if any) or have description that
	 * contains the phrase specified in the description (if any) or occur on the
	 * date specified by the user and have description that contains the phrase
	 * specified in the description. If the user doesn't pick a date, or enter a
	 * description, the method should set the errorMessageProperty to prompt the
	 * user "Please choose a date or enter a description, or both!"
	 * 
	 * if no events found satisfy the conditions, it sets the errorMessageProperty
	 * to indicate that. otherwise, it clears the eventList and add the found events
	 * to the eventList.
	 * 
	 * Create private helper methods when needed.
	 * 
	 * aDate stores the LocalDate in the date picker. description stores the
	 * description that user specified.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 */
	public void search() {

		//LocalDate aDate = this.dateProperty.get();
		//String description = this.descriptionProperty.get();

		// TODO:
		// Part 1
	}

	/**
	 * TODO: Part 1 Get the events in the calendarBook and sort them in their
	 * natural ordering and display them in the ListView.
	 * 
	 * To make the events show up in the ListView, need to clear the eventList and
	 * add sorted events to the eventList.
	 * 
	 * @precondition: none
	 * @postcondition: the eventList contains a list of events sorted in the
	 *                 specified order.
	 * 
	 * 
	 */
	public void sortByDate() {
		Collections.sort(this.calendarBookController.getCalendarBook().getEvents());
		this.eventList.clear();
		this.eventList.setAll(this.calendarBookController.getCalendarBook().getEvents());
	}

	/**
	 * TODO: Part 2 Get the events in the calendarBook and sort them by type in
	 * descending order followed by date in descending order and display them in the
	 * ListView.
	 * 
	 * @precondition: none
	 * @postcondition: the eventList contains a list of events sorted in the
	 *                 specified order.
	 * 
	 */
	public void sortByTypeDate() {
		Collections.sort(this.calendarBookController.getCalendarBook().getEvents(), new TypeDateComparator());
		this.eventList.clear();
		this.eventList.setAll(this.calendarBookController.getCalendarBook().getEvents());
	}

	private boolean isEmpty(List<Event> events) {
		if (events.size() == 0) {
			this.errorMessageProperty.set(UI.ExceptionMessages.NO_EVENT_ERROR_MESSAGE);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * Clear the calendarBook and the eventList.
	 * 
	 * @precondition: none
	 * @postcondition: calendarBook and eventList are empty.
	 */
	public void clearCalendarBookandListView() {
		this.calendarBookController.getCalendarBook().clear();
		this.eventList.clear();
	}

	/**
	 * 
	 * adds 20-30 random events to the event list
	 * 
	 * @precondition: none
	 * @postcondition: calendarBook and eventList will have 20-30 events added
	 */
	public void loadRandom() {
		this.calendarBookController.generateRandomEvents();
		this.eventList.clear();
		this.eventList.setAll(this.calendarBookController.getCalendarBook().getEvents());
	}
	
	/**
	 *  gets the earliest event for each type in the calendar book and displays them in the event list
	 * 
	 * @precondition none
	 * @postcondition the event list will be populated with at most 4 events, each being the earliest of its type
	 */
	public void getEarliestEventByType() {
		this.eventList.clear();
		this.eventList.setAll(this.calendarBookController.getEarliestEventByType());
	}
}
