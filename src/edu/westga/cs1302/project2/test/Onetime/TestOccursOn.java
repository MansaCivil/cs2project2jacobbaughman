package edu.westga.cs1302.project2.test.Onetime;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Onetime;

class TestOccursOn {

	@Test
	void occursOnSameDate() {
		Onetime event = new Onetime("important!", 360);
		LocalDate date = LocalDate.now();
		
		assertEquals(true, event.occursOn(date));
	}
	
	@Test
	void occursOnDateAfter() {
		Onetime event = new Onetime("important!", 360);
		LocalDate date = event.getDate().plusDays(4);
		
		assertEquals(false, event.occursOn(date));
	}
	
	@Test
	void occursOnDateBefore() {
		Onetime event = new Onetime("important!", 360);
		LocalDate date = event.getDate().minusDays(3);
		
		assertEquals(false, event.occursOn(date));
	}

}
