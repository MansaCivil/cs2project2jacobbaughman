package edu.westga.cs1302.project2.test.Monthly;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Monthly;

class TestOccursOn {

	@Test
	void occursOnSameDate() {
		Monthly event = new Monthly("event");
		LocalDate date = LocalDate.now();
		
		assertEquals(true, event.occursOn(date));
	}
	
	@Test
	void occursOnWeekAfter() {
		Monthly event = new Monthly("event");
		LocalDate date = event.getDate().plusDays(7);
		
		assertEquals(false, event.occursOn(date));
	}
	
	
	@Test
	void occursOnWeekBefore() {
		Monthly event = new Monthly("event");
		LocalDate date = event.getDate().minusDays(7);
		
		assertEquals(false, event.occursOn(date));
	}
	
	@Test
	void monthAfterEventDate() {
		Monthly event = new Monthly("event");
		LocalDate date = event.getDate().plusMonths(1);
		
		assertEquals(true, event.occursOn(date));
	}
	
	@Test
	void yearAfterEventDate() {
		Monthly event = new Monthly("event");
		LocalDate date = event.getDate().plusYears(1);
		
		assertEquals(true, event.occursOn(date));
	}

}
