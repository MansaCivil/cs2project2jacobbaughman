package edu.westga.cs1302.project2.test.TypeDateComparator;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.Onetime;
import edu.westga.cs1302.project2.model.TypeDateComparator;

class TestCompare {

	@Test
	void test2EventsSametypeDescendingDate() {
		Event event1 = new Daily(LocalDate.now(), "Eat");
		Event event2 = new Daily(event1.getDate().plusDays(2), "Breathe");
		List<Event> events = new ArrayList<Event>();
		events.add(event2);
		events.add(event1);
		
		Collections.sort(events, new TypeDateComparator());
		
		assertAll(
				() -> assertEquals("Eat", events.get(0).getDescription()),
				() -> assertEquals("Breathe", events.get(1).getDescription())
				);
		
	}
	
	@Test
	void test2EventsDifferentTypeIsDescending() {
		Event event1 = new Monthly(LocalDate.now(), "Eat");
		Event event2 = new Daily(event1.getDate().plusDays(2), "Breathe");
		List<Event> events = new ArrayList<Event>();
		events.add(event2);
		events.add(event1);
		
		Collections.sort(events, new TypeDateComparator());
		
		assertAll(
				() -> assertEquals("Eat", events.get(0).getDescription()),
				() -> assertEquals("Breathe", events.get(1).getDescription())
				);
	}
	
	@Test
	void testManyEvents() {
		Event event1 = new Monthly(LocalDate.now(), "Eat");
		Event event2 = new Daily(event1.getDate().plusDays(2), "Breathe");
		Event event3 = new Daily(event1.getDate().plusDays(4), "Check sensors");
		Event event4 = new Onetime(event1.getDate().plusDays(4), "Paint car", 2);
		List<Event> events = new ArrayList<Event>();
		events.add(event2);
		events.add(event1);
		events.add(event3);
		events.add(event4);
		
		Collections.sort(events, new TypeDateComparator());
		
		assertAll(
				() -> assertEquals("Paint car", events.get(0).getDescription()),
				() -> assertEquals("Eat", events.get(1).getDescription()),
				() -> assertEquals("Breathe", events.get(2).getDescription()),
				() -> assertEquals("Check sensors", events.get(3).getDescription())
				);
		
	}

}
