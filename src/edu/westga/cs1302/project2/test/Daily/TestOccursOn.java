package edu.westga.cs1302.project2.test.Daily;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;

class TestOccursOn {

	@Test
	void occursOnSameDate() {
		Daily dailyEvent = new Daily("event 1");
		LocalDate date = LocalDate.now();
		
		assertEquals(true, dailyEvent.occursOn(date));
	}
	
	@Test
	void occursOnDayAfter() {
		Daily dailyEvent = new Daily("event 1");
		LocalDate date = dailyEvent.getDate().plusDays(1);
		
		assertEquals(true, dailyEvent.occursOn(date));
	}
	
	@Test
	void occursOnDayBefore() {
		Daily dailyEvent = new Daily("event 1");
		LocalDate date = dailyEvent.getDate().minusDays(1);
		
		assertEquals(false, dailyEvent.occursOn(date));
	}
	
	@Test
	void dateBeforeEventDate() {
		Daily dailyEvent = new Daily("event 1");
		LocalDate date = LocalDate.of(1999, 2, 15);
		
		assertEquals(false, dailyEvent.occursOn(date));
	}
	
	@Test
	void dateAfterEventDate() {
		Daily dailyEvent = new Daily("event 1");
		LocalDate date = LocalDate.of(2021, 2, 15);
		
		assertEquals(true, dailyEvent.occursOn(date));
	}

}
