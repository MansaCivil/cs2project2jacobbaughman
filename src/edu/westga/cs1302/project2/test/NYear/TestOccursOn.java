package edu.westga.cs1302.project2.test.NYear;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.NYear;

class TestOccursOn {

	@Test
	void occursOnSameDate() {
		NYear event = new NYear("Recollect!", 3);
		LocalDate date = LocalDate.now();
		
		assertEquals(true, event.occursOn(date));
	}
	
	@Test
	void occursOnDateAfter() {
		NYear event = new NYear("Recollect!", 3);
		LocalDate date = event.getDate().plusDays(4);
		
		assertEquals(false, event.occursOn(date));
	}
	
	@Test
	void occursOnDateBefore() {
		NYear event = new NYear("Recollect!", 3);
		LocalDate date = event.getDate().minusDays(3);
		
		assertEquals(false, event.occursOn(date));
	}
	
	@Test
	void occursOnNYearsAfter() {
		NYear event = new NYear("Recollect!", 3);
		LocalDate date = event.getDate().plusYears(3);
		
		assertEquals(true, event.occursOn(date));
	}
	
	@Test
	void occursOnManyNYearsAfter() {
		NYear event = new NYear("Recollect!", 3);
		LocalDate date = event.getDate().plusYears(9);
		
		assertEquals(true, event.occursOn(date));
	}

	@Test
	void occursOnNYearsBefore() {
		NYear event = new NYear("Recollect!", 3);
		LocalDate date = event.getDate().minusYears(3);
		
		assertEquals(false, event.occursOn(date));
	}
}
