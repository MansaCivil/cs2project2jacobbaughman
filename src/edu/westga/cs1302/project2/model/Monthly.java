package edu.westga.cs1302.project2.model;

import java.time.LocalDate;

/**
 * this class models a Monthly event
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public class Monthly extends Event {

	/**
	 * constructs a monthly event with specified date and description
	 * 
	 * @precondition date != null && date may cannot be earlier than today
	 * 					description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == date
	 * 
	 * @param date the event's date
	 * @param description description of the event
	 * 
	 */
	public Monthly(LocalDate date, String description) {
		super(date, description);
	}
	
	/**
	 * constructs a monthly event with specified description with today's date
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == current date
	 * 
	 * @param description description of the event
	 */
	public Monthly(String description) {
		super(description);
	}

	@Override
	public boolean occursOn(LocalDate date) {
		return this.getDayOfMonth() == date.getDayOfMonth() && (date.isAfter(this.getDate()) || date.isEqual(this.getDate()));
	}
	
	@Override
	public int hashCode() {
		return super.hashCode() + this.toString().hashCode();
	}
}
