package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * this class models an event
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public abstract class Event implements Comparable<Event> {
	private LocalDate eventDate;
	private String description;
	
	/**
	 * constructs event at specified date with specified description
	 * 
	 * @precondition date != null && date may cannot be earlier than today
	 * 					description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == date
	 * 
	 * @param date the event's date
	 * @param description description of the event
	 */
	public Event(LocalDate date, String description) {
		if (date == null) {
			throw new IllegalArgumentException("Date cannot be null");
		}
		if (date.isBefore(LocalDate.now())) {
			throw new IllegalArgumentException("Date must be after current date");
		}
		if (description == null || description.isEmpty()) {
			throw new IllegalArgumentException("Invalid description");
		}
		
		this.eventDate = date;
		this.description = description;
	}
	
	/**
	 * constructs event with specified description with today's date
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == current date
	 * 
	 * @param description description of the event
	 */
	public Event(String description) {
		this(LocalDate.now(), description);
	}
	
	/**
	 * gets the date of the event
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the date
	 */
	public LocalDate getDate() {
		return this.eventDate;
	}
	
	/**
	 * gets the year of the event
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year
	 */
	public int getYear() {
		return this.eventDate.getYear();
	}
	
	/**
	 * gets the month of the event
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the month value
	 */
	public int getMonthValue() {
		return this.eventDate.getMonthValue();
	}
	
	/**
	 * gets the day value of the event
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the day value
	 */
	public int getDayOfMonth() {
		return this.eventDate.getDayOfMonth();
	}
	
	/**
	 * gets the description of the event
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public String toString() {
		return this.eventDate.toString() + ", " + this.getClass().getSimpleName() + ", " + this.description;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		if (otherObject == null) {
			return false;
		}
		if (this == otherObject) {
			return true;
		}
		if (otherObject.getClass() != this.getClass()) {
			return false;
		}
		
		Event otherEvent = (Event) otherObject;
		
		return otherEvent.getDescription().equals(this.description) && otherEvent.getDate().isEqual(this.eventDate);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.eventDate, this.description);
	}
	
	@Override
	public int compareTo(Event otherEvent) {
		return this.eventDate.compareTo(otherEvent.getDate());
	}
	
	/**
	 * checks if the event occurs on a specified date
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param date the specified date
	 * 
	 * @return true if the event occurs on this date, false otherwise
	 */
	public abstract boolean occursOn(LocalDate date);
}
