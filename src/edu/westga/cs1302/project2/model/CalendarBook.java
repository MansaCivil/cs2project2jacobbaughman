package edu.westga.cs1302.project2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * the class models a calendar book
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public class CalendarBook {
	private List<Event> events;
	
	/**
	 * constructs a default calendar book
	 * 
	 * @precondition none
	 * @postcondition ready to store events
	 */
	public CalendarBook() {
		this.events = new ArrayList<Event>();
	}
	
	/**
	 * initializes a default calendar book with a specified list of events
	 * 
	 * @precondition events != null
	 * @postcondition getEvents() == events
	 * 
	 * @param events the list of events
	 */
	public CalendarBook(List<Event> events) {
		if (events == null) {
			throw new IllegalArgumentException("List of events cannot be null");
		}
		this.events = events;
	}
	
	/**
	 * gets number of events in calendar book
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of events
	 */
	public int size() {
		return this.events.size();
	}
	
	/**
	 * gets the list of events
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the events
	 */
	public List<Event> getEvents() {
		return this.events;
	}
	
	/**
	 * adds event to the calendar
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param event the event to add
	 * 
	 * @return true if the event is added, false if the event is a duplicate or null
	 */
	public boolean add(Event event) {
		if (event == null) {
			return false;
		}
		if (this.contains(event)) {
			return false;
		}
		
		return this.events.add(event);
	}
	
	/**
	 * checks if an event is already in the calendar
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param event the event to check for
	 * 
	 * @return true if it is already in the calendar, false otherwise
	 */
	public boolean contains(Event event) {
		for (Event anEvent : this.events) {
			if (anEvent.equals(event)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * removes all events from calendar
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 * 
	 */
	public void clear() {
		this.events.clear();
	}
}	
