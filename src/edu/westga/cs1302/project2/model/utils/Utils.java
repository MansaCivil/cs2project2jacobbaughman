package edu.westga.cs1302.project2.model.utils;

import java.time.LocalDate;

import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.NYear;
import edu.westga.cs1302.project2.model.Onetime;

/**
 * utility class
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 *
 */
public class Utils {
	
	/**
	 * creates an event with specified parameters
	 * 
	 * @precondition date != null && date may cannot be earlier than today
	 * 					description != null && !description.isEmpty()
	 * @postcondition none
	 * 
	 * @param type the type of event
	 * @param date the date of the event
	 * @param description description of the event
	 * @param nyear number of years for nyear event
	 * @param siglevel the significance of a onetime event
	 * 
	 * @return the event, a onetime event of siglevel 1 if type is not defined in EventType
	 */
	public static Event createEvent(String type, LocalDate date, String description, int nyear, int siglevel) {
		try {
			EventType eventType = EventType.valueOf(type.toUpperCase());
			
			if (eventType == EventType.DAILY) {
				return new Daily(date, description);
				
			} else if (eventType == EventType.MONTHLY) {
				return new Monthly(date, description);
				
			} else if (eventType == EventType.ONETIME) {
				return new Onetime(date, description, siglevel);
				
			} else {
				return new NYear(date, description, nyear);
			}
			
		} catch (Exception exception) {
			return new Onetime(date, description, 1);
		}
		
	}
}
