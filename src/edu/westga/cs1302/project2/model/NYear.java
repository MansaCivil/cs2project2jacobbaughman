package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.time.Period;

/**
 * models an event that occurs every n years
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 *
 */
public class NYear extends Event {
	private int nYears;
	
	/**
	 * constructs a nyear event with specified date, description, and n
	 * 
	 * @precondition date != null && date may cannot be earlier than today
	 * 					description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == date
	 * 
	 * @param date the event's date
	 * @param description description of the event
	 * @param nYears number of years between occurrences
	 * 
	 */
	public NYear(LocalDate date, String description, int nYears) {
		super(date, description);
		this.nYears = nYears;
	}
	
	/**
	 * constructs a nyear event with specified description and n on today's date
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == current date
	 * 
	 * @param description description of the event
	 * @param nYears number of years
	 */
	public NYear(String description, int nYears) {
		this(LocalDate.now(), description, nYears);
	}
	
	/**
	 * gets the number of years
	 * 
	 * @return the number of years
	 */
	public int getN() {
		return this.nYears;
	}
	
	@Override
	public boolean occursOn(LocalDate date) {
		if (date.isBefore(this.getDate())) {
			return false;
		}
		
		Period timeBetween = this.getDate().until(date);
		return timeBetween.getDays() == 0 && timeBetween.getMonths() == 0 && timeBetween.getYears() % this.nYears == 0;

	}
	
	@Override
	public String toString() {
		return super.toString() + ", N = " + this.nYears;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode() + this.toString().hashCode();
	}
	
}
