package edu.westga.cs1302.project2.model;

import java.time.LocalDate;

/**
 * this class models a onetime event
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public class Onetime extends Event {
	private int sigLevel;

	/**
	 * constructs a onetime event with specified date, description, and significance
	 * 
	 * @precondition date != null && date may cannot be earlier than today
	 * 					description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == date
	 * 
	 * @param date the event's date
	 * @param description description of the event
	 * @param sigLevel the significance level of the event
	 * 
	 */
	public Onetime(LocalDate date, String description, int sigLevel) {
		super(date, description);
		this.sigLevel = sigLevel;
	}
	
	/**
	 * constructs a onetime event with specified description and significance on today's date
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == current date
	 * 
	 * @param description description of the event
	 * @param sigLevel significance of event
	 */
	public Onetime(String description, int sigLevel) {
		this(LocalDate.now(), description, sigLevel);
	}
	
	/**
	 * gets significance of the event
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the significance level
	 */
	public int getSigLevel() {
		return this.sigLevel;
	}

	@Override
	public boolean occursOn(LocalDate date) {
		return this.getDate().isEqual(date);
	}
	
	@Override
	public String toString() {
		return super.toString() + ", significance level = " + this.sigLevel;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode() + this.toString().hashCode();
	}
}
