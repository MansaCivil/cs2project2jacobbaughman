package edu.westga.cs1302.project2.model;

import java.time.LocalDate;

/**
 * this class models a daily event
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public class Daily extends Event {
	
	/**
	 * constructs a daily event with specified date and description
	 * 
	 * @precondition date != null && date may cannot be earlier than today
	 * 					description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == date
	 * 
	 * @param date the event's date
	 * @param description description of the event
	 * 
	 */
	public Daily(LocalDate date, String description) {
		super(date, description);
	}
	
	/**
	 * constructs a daily event with specified description with today's date
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description, getDate() == current date
	 * 
	 * @param description description of the event
	 */
	public Daily(String description) {
		super(description);
	}

	@Override
	public boolean occursOn(LocalDate date) {
		return this.getDate().equals(date) || date.isAfter(this.getDate());
	}
	
	@Override
	public int hashCode() {
		return super.hashCode() + this.toString().hashCode();
	}
}
