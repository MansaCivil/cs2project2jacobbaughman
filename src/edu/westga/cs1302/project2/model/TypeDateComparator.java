package edu.westga.cs1302.project2.model;

import java.util.Comparator;

/**
 * comparator for sorting events by descending type, then by descending date
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 *
 */
public class TypeDateComparator implements Comparator<Event> {

	@Override
	public int compare(Event event1, Event event2) {
		String event1Type = event1.getClass().getSimpleName();
		String event2Type = event2.getClass().getSimpleName();
		
		int typeCompare = event2Type.compareTo(event1Type);
		
		if (typeCompare != 0) {
			return typeCompare;
		} else {
			return event1.compareTo(event2);
		}
	}

}
