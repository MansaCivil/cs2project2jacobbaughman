package edu.westga.cs1302.project2.model;

/**
 * enumeration of event types
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public enum EventType {
	DAILY,
	MONTHLY,
	NYEAR,
	ONETIME
}
