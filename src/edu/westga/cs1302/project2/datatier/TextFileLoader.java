package edu.westga.cs1302.project2.datatier;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.utils.Utils;

/**
 * text file loader class
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 *
 */
public class TextFileLoader implements TextLoader {
	private File aTextFile;
	
	/**
	 * constructs a text file loader with specified file
	 * 
	 * @precondition none
	 * @postcondition can load data from aTextFile
	 * 
	 * @param aTextFile the file to load text from
	 */
	public TextFileLoader(File aTextFile) {
		this.aTextFile = aTextFile;
	}
	
	@Override
	public List<Event> loadData() throws IOException {
		List<Event> events = new ArrayList<Event>();
		
		try (Scanner input = new Scanner(this.aTextFile)) {
			input.useDelimiter("\r\n|\n");
			while (input.hasNext()) {
				String currentLine = input.next();
				try {
					String[] currentPieces = currentLine.split(",");
					String type = currentPieces[0];
					int sigOrN = 0;
					
					if (type.equalsIgnoreCase(EventType.ONETIME.toString()) || type.equalsIgnoreCase(EventType.NYEAR.toString())) {
						sigOrN = Integer.parseInt(currentPieces[1]);
						events.add(Utils.createEvent(type, LocalDate.parse(currentPieces[2]), currentPieces[3], sigOrN, sigOrN));
					} else {
						events.add(Utils.createEvent(type, LocalDate.parse(currentPieces[1]), currentPieces[2], sigOrN, sigOrN));
					}
					
				} catch (Exception exception) {
					System.err.println("Error reading file: " + exception.getMessage() + " " + currentLine);
				}
			}
		}
		return events;
	}

}
