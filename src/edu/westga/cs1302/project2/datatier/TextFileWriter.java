package edu.westga.cs1302.project2.datatier;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;

/**
 * class for writing events to a file
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public class TextFileWriter {
	private File file;
	
	/**
	 * constructs a text file writer with a specified file
	 * 
	 * @precondition none
	 * @postcondition file is ready to save events to
	 * 
	 * @param file the file to write to
	 */
	public TextFileWriter(File file) {
		this.file = file;
	}
	
	/**
	 * saves events to file
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param events the events to save to file
	 */
	public void save(List<Event> events) throws IOException {
		try (PrintWriter output = new PrintWriter(new FileWriter(this.file))) {
			for (Event anEvent : events) {
				String date = anEvent.getDate().toString();
				String className = anEvent.getClass().getSimpleName().toUpperCase();
				String description = anEvent.getDescription();
				
				output.println(date + ", " + className + ", " + description);
			}
		}
	}
	
	/**
	 * saves events to files categorized by type
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param events the events to save to file
	 */
	public void save2(List<Event> events) throws IOException {
		try (PrintWriter output = new PrintWriter(new FileWriter(this.file))) {
			for (EventType type : EventType.values()) {
				ArrayList<Event> eventsOfType = new ArrayList<Event>();
				for (Event anEvent : events) {
					if (anEvent.getClass().getSimpleName().equalsIgnoreCase(type.toString())) {
						eventsOfType.add(anEvent);
					}
				}
				
				output.println(type.toString() + ", " + "size = " + eventsOfType.size());
				for (Event anEvent : eventsOfType) {
					output.println("\t" + anEvent.getDate().toString() + ", " + anEvent.getDescription());
				}
				output.println();
				
			}
		}
	}
	
	/**
	 * saves events to files categorized by date and type
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param events the events to save to file
	 */
	public void save3(List<Event> events) throws IOException {
		ArrayList<Event> eventsCopy = new ArrayList<Event>();
		eventsCopy.addAll(events);
		Collections.reverse(eventsCopy);
		
		try (PrintWriter output = new PrintWriter(new FileWriter(this.file))) {
			while (eventsCopy.size() > 0) {
				ArrayList<Event> eventsOfDate = new ArrayList<Event>();
				LocalDate currentDate = eventsCopy.get(eventsCopy.size() - 1).getDate();
				
				output.println("Events on " + currentDate.toString());
				int eventNumber = 1;
				for (int count = eventsCopy.size() - 1; count >= 0; count--) {
					Event currentEvent = eventsCopy.get(count);
					if (currentEvent.getDate().equals(currentDate)) {
						output.println("Event " + eventNumber + ":" + currentEvent.getDescription());
						eventsOfDate.add(currentEvent);
						eventsCopy.remove(count);
						eventNumber++;
					}
				}
				
				output.println("~~~GROUPED BY EVENT TYPE~~~");
				for (EventType type : EventType.values()) {
					ArrayList<Event> eventsOfType = new ArrayList<Event>();
					for (Event anEvent : eventsOfDate) {
						if (anEvent.getClass().getSimpleName().equalsIgnoreCase(type.toString())) {
							eventsOfType.add(anEvent);
						}
					}
					
					output.println("\t" + type.toString() + ", " + "size = " + eventsOfType.size());
					for (Event anEvent : eventsOfType) {
						output.println("\t\t" + anEvent.getDate().toString() + ", " + anEvent.getDescription());
					}
				}
				output.println();
			}
		}
	}
}
