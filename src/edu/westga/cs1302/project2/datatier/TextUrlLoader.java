package edu.westga.cs1302.project2.datatier;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.utils.Utils;

/**
 * this class loads text from a url
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 */
public class TextUrlLoader implements TextLoader {
	private URL textResource;
	
	/**
	 * constructs a text url loader
	 * 
	 * @precondition none
	 * @postcondition ready to read data from url
	 * 
	 * @param textResource the url to load from
	 */
	public TextUrlLoader(URL textResource) {
		this.textResource = textResource;
	}
	
	@Override
	public List<Event> loadData() throws IOException {
		List<Event> events = new ArrayList<Event>();
		
		try (Scanner input = new Scanner(this.textResource.openStream())) {
			input.useDelimiter("\r\n|\n");
			while (input.hasNext()) {
				String currentLine = input.next();
				try {
					String[] currentPieces = currentLine.split(" ");
					String type = currentPieces[0];
					int sigOrN = 0;
					
					if (type.equalsIgnoreCase(EventType.ONETIME.toString()) || type.equalsIgnoreCase(EventType.NYEAR.toString())) {
						sigOrN = Integer.parseInt(currentPieces[1]);
						String description = this.loadDescription(3, currentPieces);
						events.add(Utils.createEvent(type, LocalDate.parse(currentPieces[2]), description, sigOrN, sigOrN));
					} else {
						String description = this.loadDescription(2, currentPieces);
						events.add(Utils.createEvent(type, LocalDate.parse(currentPieces[1]), description, sigOrN, sigOrN));
					}
					
				} catch (Exception exception) {
					System.err.println("Error reading file: " + exception.getMessage() + " " + currentLine);
				}
			}
		}
		return events;
	}

	private String loadDescription(int start, String[] currentPieces) {
		String description = "";
		for (int count = start; count < currentPieces.length; count++) {
			if (count == currentPieces.length - 1) {
				description += currentPieces[count];
			} else {
				description += currentPieces[count] + " ";
			}
			
		}
		
		return description;
	}
}
