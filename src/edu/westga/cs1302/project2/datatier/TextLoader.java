package edu.westga.cs1302.project2.datatier;

import java.io.IOException;
import java.util.List;

import edu.westga.cs1302.project2.model.Event;

/**
 * the text loading interface
 * 
 * @author Jacob Baughman
 * @version Fall 2020
 *
 */
public interface TextLoader {

	/**
	 * load events from data
	 * 
	 * @return the list of events from the data
	 * @throws IOException
	 */
	List<Event> loadData() throws IOException;
}
