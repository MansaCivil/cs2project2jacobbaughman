package edu.westga.cs1302.project2.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.westga.cs1302.project2.model.CalendarBook;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.utils.Utils;

/**
 * CalendarBookController generates random events and gets events grouped by
 * types.
 * 
 * @author	CS1302
 * @version	Fall 2020
 *
 */
public class CalendarBookController {

	private static String[] descriptions = { "dental appointment", "car maintenance", "dance lessons",
		"drawing lessongs", "birthday party", "grocery shopping" };

	private CalendarBook calendarBook;

	/**
	 * Instantiates a new calendar book controller.
	 *
	 * @precondition: none
	 * @postcondition: getCalendarBook().size()==0
	 */
	public CalendarBookController() {
		this.calendarBook = new CalendarBook();
	}

	/**
	 * Part 3 TODO: Generate random events and add them into the caldenarBook.
	 * 
	 * Randomly generate a number between 20 and 30. Create that number of Onetime,
	 * Monthly, Daily and NYear events
	 * 
	 * For each event, the date should be a random number within 0-364 days after
	 * today, the description should be any random description in the descriptions
	 * array, the nyear should be a random number b/w 1 to the length of the
	 * descriptions array, the sigLevel should be a random number b/w 1- 10.
	 * 
	 * @precondition: none
	 * @postcondition: calendarBook has 20-30 randomly generated events of each type
	 * 
	 */
	public void generateRandomEvents() {
		int numEvents = this.getRandomInteger(20, 31);
		
		for (int count = 0; count < numEvents; count++) {
			EventType randomType = EventType.values()[this.getRandomInteger(0, EventType.values().length)];
			LocalDate randomDate = LocalDate.now().plusDays(this.getRandomInteger(0, 365));
			String randomDescription = CalendarBookController.descriptions[this.getRandomInteger(0, 
					CalendarBookController.descriptions.length)];
			int randomNYear = this.getRandomInteger(1, CalendarBookController.descriptions.length + 1);
			int randomSigLevel = this.getRandomInteger(1, 11);
			
			this.calendarBook.add(Utils.createEvent(randomType.toString(), randomDate, randomDescription, randomNYear, randomSigLevel));	
		}
	}
	
	private int getRandomInteger(int min, int max) {
		int range = max - min;
		
		Random rand = new Random();
		int randomInt = rand.nextInt(range) + min;
		return randomInt;
	}

	/**
	 * gets the earliest event for each event type in the calendar book
	 * 
	 * @return the earliest events for each event type
	 */
	public List<Event> getEarliestEventByType() {
		List<Event> earliestEvents = new ArrayList<Event>();
		
		for (EventType type : EventType.values()) {
			String currentType = type.toString();
			Event earliestEvent = null;
			for (Event anEvent : this.calendarBook.getEvents()) {
				boolean isOfEventType = anEvent.getClass().getSimpleName().equalsIgnoreCase(currentType);
				if (isOfEventType && (earliestEvent == null || anEvent.getDate().isBefore(earliestEvent.getDate()))) {
					earliestEvent = anEvent;
				}
			}
			if (earliestEvent != null) {
				earliestEvents.add(earliestEvent);
			}
		}
		
		return earliestEvents;
	}
		
	/**
	 * Gets the calendar book.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the calendar book
	 */
	public CalendarBook getCalendarBook() {
		return this.calendarBook;
	}

}
